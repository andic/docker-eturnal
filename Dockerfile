ARG ARG_NAME=eturnal
ARG ARG_VERSION=1.8.3


FROM ejabberd/mix as builder

ARG ARG_NAME
ARG ARG_VERSION
ENV NAME=${ARG_NAME}
ENV VERSION=${ARG_VERSION}
ENV MIX_ENV=prod

RUN set -ex \
  && echo "Building ${NAME} - ${VERSION}" \
  && curl https://eturnal.net/download/eturnal-${VERSION}.tar.gz | tar -C /tmp -xzf - \
  && cd /tmp/eturnal-${VERSION} \
  && ./rebar3 as prod tar \
  && mkdir -p /opt/eturnal \
  && cd /opt/eturnal \
  && tar -xzf /tmp/eturnal-${VERSION}/_build/prod/rel/eturnal/eturnal-${VERSION}.tar.gz \
  && rm /tmp/eturnal-${VERSION}/_build/prod/rel/eturnal/eturnal-${VERSION}.tar.gz \
  && echo "Done building ${NAME} - ${VERSION}"


## Runtime container
FROM alpine:3.16

LABEL org.opencontainers.image.authors="Andreas Cislik <git@a11k.net>"
LABEL org.opencontainers.image.source="https://gitlab.com/andic/docker-eturnal"
LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.title="eturnal"
LABEL org.opencontainers.image.description="STUN / TURN standalone server"

ARG ARG_NAME
ARG ARG_VERSION
ENV NAME=${ARG_NAME}
ENV VERSION=${ARG_VERSION}
ENV ETURNAL_USER=eturnal
ENV ETURNAL_BIN_PREFIX=/opt/eturnal
ENV ETURNAL_ETC_PREFIX=/etc/eturnal
ENV ETURNAL_SECRET=long-and-cryptic
ENV LOGS_DIRECTORY=stdout
ENV RUNTIME_DIRECTORY=run 
ENV ETURNAL_REALM=eturnal.net
ENV ETURNAL_RELAY_IPV4_ADDR=203.0.113.42
ENV ETURNAL_RELAY_IPV6_ADDR=2001:db8::42
ENV ETURNAL_ENABLE_TURN_UDP=true
ENV ETURNAL_ENABLE_TURN_TCP=true
ENV ETURNAL_ENABLE_TURN_TLS=false
ENV ETURNAL_TLS_CRT_FILE=/etc/eturnal/tls/crt.pem
ENV ETURNAL_TLS_KEY_FILE=/etc/eturnal/tls/key.pem
ENV ETURNAL_RELAY_MIN_PORT=49152
ENV ETURNAL_RELAY_MAX_PORT=65535    
ENV ETURNAL_BLACKLIST='["127.0.0.0/8", "::1", "2001::/32", "2002::/16"]'
ENV ETURNAL_LOG_LEVEL=info
ENV ETURNAL_MODULES='modules: {mod_log_stun: {}}'

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]
#VOLUME /etc/eturnal
EXPOSE 3478

WORKDIR /opt/eturnal
COPY --from=builder /opt/eturnal .
COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
COPY eturnal-template.yml /usr/local/templates/eturnal-template.yml
COPY eturnal-tls-template.yml /usr/local/templates/eturnal-tls-template.yml

RUN set -ex \
  && echo "Installing ${NAME} - ${VERSION}" \
  && apk update \
  && apk upgrade \
  && apk add \
    gettext \
    libstdc++ \
    ncurses-libs \
    openssl \
    tzdata \
    yaml \
  && rm -rf /var/cache/apk/* \
  && chmod 755 /usr/local/bin/docker-entrypoint.sh \
  && addgroup eturnal -g 1000 \
  && adduser -u 1000 -G eturnal -D eturnal \
  && echo "Done installing ${NAME} - ${VERSION}" \
