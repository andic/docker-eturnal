#!/bin/sh

echo "starting docker-entrypoint.sh"
echo "$(date)"
echo "${NAME} - ${VERSION}"


## create config
if [ ! -e ${ETURNAL_ETC_PREFIX}/eturnal.yml ] && ( [ ${ETURNAL_ENABLE_TURN_TLS} == "True" ] || [ ${ETURNAL_ENABLE_TURN_TLS} == "true" ] ); then
  envsubst < /usr/local/templates/eturnal-tls-template.yml > ${ETURNAL_ETC_PREFIX}/etc/eturnal.yml
else
  envsubst < /usr/local/templates/eturnal-template.yml > ${ETURNAL_ETC_PREFIX}/etc/eturnal.yml
fi


## start service
${ETURNAL_BIN_PREFIX}/bin/eturnalctl foreground


echo "stopping docker-entrypoint.sh"
echo "$(date)"
